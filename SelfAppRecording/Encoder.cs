﻿using Microsoft.Graphics.Canvas;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Media.Core;
using Windows.Media.MediaProperties;
using Windows.Media.Transcoding;
using Windows.Security.Cryptography;
using Windows.Storage.Streams;
using Windows.UI;
using System.Runtime.InteropServices;
using Windows.Graphics.Imaging;

namespace SelfAppRecording
{

    [ComImport]
    [Guid("5B0D3235-4DBA-4D44-865E-8F1D0E4FD04D")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    unsafe interface IMemoryBufferByteAccess
    {
        void GetBuffer(out byte* buffer, out uint capacity);
    }
    public class Encoder
    {

        private CanvasDevice _device;
        private VideoStreamDescriptor _videoDescriptor;
        private MediaStreamSource _mediaStreamSource;
        private MediaTranscoder _transcoder;
        private bool _isRecording;
        private bool _closed = false;
        private uint _sourceWidth;
        private uint _sourceHeight;
        BitmapToDirect3DHelper _bitmapToDirect3DHelper;



        // R8G8B8A8 is a 32 bit directx pixel format, so bits per frame would be 32;
        const uint BitsPerPixel = 32;

        public Encoder(BitmapToDirect3DHelper bitmapHelper, uint sourceWidth, uint sourceHeight)
        {
            _bitmapToDirect3DHelper = bitmapHelper;
            _device = CanvasDevice.GetSharedDevice();
            _isRecording = false;
            _sourceWidth = sourceWidth;
            _sourceHeight = sourceHeight;
            CreateMediaObjects();
        }



        internal IAsyncAction EncodeAsync(IRandomAccessStream stream, uint framesPerSecond)
        {
            uint bitrateInBps = BitsPerPixel * _sourceWidth * _sourceHeight / framesPerSecond;
            return EncodeInternalAsync(stream, _sourceWidth, _sourceHeight, bitrateInBps, framesPerSecond).AsAsyncAction();
        }

        private async Task EncodeInternalAsync(IRandomAccessStream stream, uint width, uint height, uint bitrateInBps, uint frameRate)
        {
            if (!_isRecording)
            {
                _isRecording = true;

                var encodingProfile = new MediaEncodingProfile();
                encodingProfile.Container.Subtype = "MPEG4";
                encodingProfile.Video.Subtype = "H264";
                encodingProfile.Video.Width = width;
                encodingProfile.Video.Height = height;
                encodingProfile.Video.Bitrate = bitrateInBps;
                encodingProfile.Video.FrameRate.Numerator = frameRate;
                encodingProfile.Video.FrameRate.Denominator = 1;
                encodingProfile.Video.PixelAspectRatio.Numerator = 1;
                encodingProfile.Video.PixelAspectRatio.Denominator = 1;
                var transcode = await _transcoder.PrepareMediaStreamSourceTranscodeAsync(_mediaStreamSource, stream, encodingProfile);

                await transcode.TranscodeAsync();
            }
        }

        public void Dispose()
        {
            if (_closed)
            {
                return;
            }
            _closed = true;

            if (!_isRecording)
            {
                DisposeInternal();
            }

            _isRecording = false;
        }

        private void DisposeInternal()
        {

        }

        private void CreateMediaObjects()
        {


            // Describe our input: uncompressed BGRA8 buffers
            var videoProperties = VideoEncodingProperties.CreateUncompressed(MediaEncodingSubtypes.Bgra8, _sourceWidth, _sourceHeight);
            _videoDescriptor = new VideoStreamDescriptor(videoProperties);


            // Create our MediaStreamSource
            _mediaStreamSource = new MediaStreamSource(_videoDescriptor);
            _mediaStreamSource.BufferTime = TimeSpan.FromSeconds(0);
            _mediaStreamSource.Starting += OnMediaStreamSourceStarting;
            _mediaStreamSource.SampleRequested += OnMediaStreamSourceSampleRequested;

            // Create our transcoder
            _transcoder = new MediaTranscoder();
            _transcoder.HardwareAccelerationEnabled = true;

        }

        private void OnMediaStreamSourceSampleRequested(MediaStreamSource sender, MediaStreamSourceSampleRequestedEventArgs args)
        {
            if (_isRecording && !_closed)
            {
                // do off-screen rendering on the other frame so that it can be used later

                // There's at least one frame guaranteed


                // Wait for pool before trying to render
                while (_bitmapToDirect3DHelper.frameTouse == default)
                {

                }
                FrameData frameData = _bitmapToDirect3DHelper.frameTouse;

                IBuffer flippedBitmap = FlipBitmap(frameData);
                args.Request.Sample = MediaStreamSample.CreateFromBuffer(flippedBitmap, frameData.RelativeTime);
                //var deferral = args.Request.GetDeferral();
                //await CoreApplication.MainView.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.High, () =>

                //{
                //    float currentDpi = Windows.Graphics.Display.DisplayInformation.GetForCurrentView().LogicalDpi;
                //    using (CanvasBitmap canvas = CanvasBitmap.CreateFromBytes(CanvasDevice.GetSharedDevice(), frameData.Bitmap,
                //                       frameData.PixelWidth, frameData.PixelHeight,
                //                       Windows.Graphics.DirectX.DirectXPixelFormat.B8G8R8A8UIntNormalized, currentDpi))
                //    {
                //        using (CanvasRenderTarget rendertarget = new CanvasRenderTarget(CanvasDevice.GetSharedDevice(), canvas.SizeInPixels.Width, canvas.SizeInPixels.Height, currentDpi))
                //        {
                //            using (CanvasDrawingSession ds = rendertarget.CreateDrawingSession())
                //            {
                //                ds.Clear(Colors.Black);
                //                ds.DrawImage(canvas);
                //            }
                //            //args.Request.Sample = MediaStreamSample.CreateFromDirect3D11Surface(rendertarget, frameData.RelativeTime);
                //            var bytes = rendertarget.GetPixelBytes();
                //            IBuffer buffer = CryptographicBuffer.CreateFromByteArray(bytes);
                //            args.Request.Sample = MediaStreamSample.CreateFromBuffer(buffer, frameData.RelativeTime);
                //        }
                //    }
                //});
                //deferral.Complete();

            }



            else
            {
                // Basically ends the stream.
                Debug.WriteLine("Death");
                args.Request.Sample = null;
            }
        }

        private unsafe IBuffer FlipBitmap(FrameData frameData)
        {
            IBuffer flippedBitmapBuffer = new Windows.Storage.Streams.Buffer((uint)(frameData.PixelWidth * frameData.PixelHeight * BitsPerPixel));
            SoftwareBitmap softwareBitmap = SoftwareBitmap.CreateCopyFromBuffer(frameData.Bitmap, BitmapPixelFormat.Bgra8,
                frameData.PixelWidth, frameData.PixelHeight);

            using (BitmapBuffer buffer = softwareBitmap.LockBuffer(BitmapBufferAccessMode.ReadWrite))
            {
                using (var reference = buffer.CreateReference())
                {
                    byte* dataInBytes;
                    byte[] copy = new byte[BitsPerPixel * frameData.PixelWidth * frameData.PixelHeight];
                    uint capacity;
                    ((IMemoryBufferByteAccess)reference).GetBuffer(out dataInBytes, out capacity);

                    // Fill copy of BGRA plane
                    BitmapPlaneDescription bufferLayout = buffer.GetPlaneDescription(0);
                    
                    // PixelWidth * 4 channels
                    int HeightOffset = bufferLayout.Width * 4;

                    for (int i = 0; i < bufferLayout.Height; i++)
                    {
                        for (int j = 0; j < bufferLayout.Width; j++)
                        {
                            copy[i * HeightOffset + 4 * j + 0] = dataInBytes[bufferLayout.StartIndex + bufferLayout.Stride * i + 4 * j + 0];
                            copy[i * HeightOffset + 4 * j + 1] = dataInBytes[bufferLayout.StartIndex + bufferLayout.Stride * i + 4 * j + 1];
                            copy[i * HeightOffset + 4 * j + 2] = dataInBytes[bufferLayout.StartIndex + bufferLayout.Stride * i + 4 * j + 2];
                            copy[i * HeightOffset + 4 * j + 3] = dataInBytes[bufferLayout.StartIndex + bufferLayout.Stride * i + 4 * j + 3];
                        }
                    }

                    // Flip BGRA plane using copy
                    for (int i = 0; i < bufferLayout.Height; i++)
                    {
                        int heightValue = (bufferLayout.Height - i - 1) * HeightOffset;
                        for (int j = 0; j < bufferLayout.Width; j++)
                        {
                            // There are 4 colour channels
                            int widthValue = j * 4;
                            dataInBytes[bufferLayout.StartIndex + bufferLayout.Stride * i + 4 * j + 0] = 
                                copy[heightValue + widthValue + 0];

                            dataInBytes[bufferLayout.StartIndex + bufferLayout.Stride * i + 4 * j + 1] =
                                copy[heightValue + widthValue + 1];

                            dataInBytes[bufferLayout.StartIndex + bufferLayout.Stride * i + 4 * j + 2] =
                                copy[heightValue + widthValue + 2];

                            dataInBytes[bufferLayout.StartIndex + bufferLayout.Stride * i + 4 * j + 3] =
                                copy[heightValue + widthValue + 3];
                        }
                    }
                }

            }
            softwareBitmap.CopyToBuffer(flippedBitmapBuffer);
            return flippedBitmapBuffer;
        }

        private void OnMediaStreamSourceStarting(MediaStreamSource sender, MediaStreamSourceStartingEventArgs args)
        {
            while (_bitmapToDirect3DHelper.frameTouse == default)
            {

            }
            var firstSample = _bitmapToDirect3DHelper.frameTouse;
            args.Request.SetActualStartPosition(firstSample.RelativeTime);
            Debug.WriteLine(firstSample.RelativeTime);
        }


        internal void Stop()
        {
            _isRecording = false;
            _bitmapToDirect3DHelper.frameTouse = default;
        }
    }
}
