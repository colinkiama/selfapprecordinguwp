﻿using System;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;

namespace SelfAppRecording
{
    public struct FrameData
    {
        public IBuffer Bitmap { get; set; }
        public TimeSpan RelativeTime { get; set; }
        public int PixelWidth { get; set; }
        public int PixelHeight { get; set; }


        public static bool operator ==(FrameData left, FrameData right)
        {
            return left.Bitmap == right.Bitmap && left.RelativeTime == right.RelativeTime 
                && left.PixelWidth == right.PixelWidth;
        }

        public static bool operator !=(FrameData left, FrameData right)
        {
            return !(left == right);
        }
    }
}