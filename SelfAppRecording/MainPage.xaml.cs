﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace SelfAppRecording
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        Encoder _encoder;
        Stopwatch stopwatch = new Stopwatch();
        BitmapToDirect3DHelper bitmapToDirect3DHelper = new BitmapToDirect3DHelper();
        Size resolutionSize = new Size(1, 1);
        //CompositionExportHelper exportHelper = new CompositionExportHelper();
        const uint MillisecondsInSeconds = 1000;
        uint framesPerSecond = 15;
        List<uint> FPSList = new List<uint> { 15, 30, 60 };
        Dictionary<string, Size> resolutionDictionary = new Dictionary<string, Size>();

        List<string> screenResolutionList = new List<string>()
        {
           "Native Resolution",
           "1080P",
           "720P",
           "480P",
           "360P",
        };


        TimeSpan intervalTime;



        bool hasTimerStopped = false;

        public MainPage()
        {
            this.InitializeComponent();
            FillResolutionDictionary();

        }

        private void ExportHelper_CompletedExport(object sender, EventArgs e)
        {
            RecordButton.IsEnabled = true;
        }

        private void FillResolutionDictionary()
        {
            resolutionDictionary.Add("1080P", new Size(1920, 1080));
            resolutionDictionary.Add("720P", new Size(1280, 720));
            resolutionDictionary.Add("480P", new Size(854, 480));
            resolutionDictionary.Add("360P", new Size(640, 360));
        }

        private void BitmapToDirect3DHelper_CancellationStarted(object sender, EventArgs e)
        {
            StopButton.IsEnabled = false;
        }


        private async void RecordButton_Click(object sender, RoutedEventArgs e)
        {
            hasTimerStopped = false;

            bool isDifferentFromNativeResolution = false;
            framesPerSecond = FPSList[FPSComboBox.SelectedIndex];


            if (ResolutionComboBox.SelectedIndex != 0)
            {
                isDifferentFromNativeResolution = true;
                resolutionSize = DetermineResolutionSize();
                _encoder = new Encoder(bitmapToDirect3DHelper, (uint)resolutionSize.Width, (uint)resolutionSize.Height);
            }
            else
            {
                RenderTargetBitmap testBitmap = new RenderTargetBitmap();
                await testBitmap.RenderAsync(null);
                resolutionSize = new Size(testBitmap.PixelWidth, testBitmap.PixelHeight);
                _encoder = new Encoder(bitmapToDirect3DHelper, (uint)resolutionSize.Width, (uint)resolutionSize.Height);
            }
            intervalTime = TimeSpan.FromMilliseconds(1d / framesPerSecond * MillisecondsInSeconds);
            RecordButton.IsEnabled = false;
            StopButton.IsEnabled = true;


            // Runs in a background thread so UI doesn't freeze up
            _ = Task.Run(async () =>
              {
                  stopwatch.Start();
                  TimeSpan lag = TimeSpan.Zero;
                  TimeSpan totalTimeElapsed = TimeSpan.Zero;
                  long intervalTimeInTicks = intervalTime.Ticks;
                  long lastElapsedTime = 0;
                  while (!hasTimerStopped)
                  {
                      //Debug.WriteLine(stopwatch.ElapsedMilliseconds);

                      long currentElapsedTime = stopwatch.ElapsedTicks;
                      if (lastElapsedTime > currentElapsedTime)
                      {
                          totalTimeElapsed += new TimeSpan(currentElapsedTime);
                      }
                      else
                      {
                          totalTimeElapsed += new TimeSpan(currentElapsedTime - lastElapsedTime);
                      }
                      lastElapsedTime = currentElapsedTime;

                      if (stopwatch.ElapsedTicks >= intervalTimeInTicks)
                      {
                          lag = TimeSpan.FromTicks(currentElapsedTime - intervalTimeInTicks);
                          stopwatch.Restart();

                          // Code that requires the UI thread runs here
                          await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.High, async () =>
                            {

                                RenderTargetBitmap renderTargetBitmap = new RenderTargetBitmap();
                                if (isDifferentFromNativeResolution)
                                {
                                    await renderTargetBitmap.RenderAsync(null, (int)resolutionSize.Width, (int)resolutionSize.Height);
                                }
                                else
                                {
                                    await renderTargetBitmap.RenderAsync(null);
                                }
                                FrameData frameData = new FrameData
                                {
                                    PixelWidth = renderTargetBitmap.PixelWidth,
                                    PixelHeight = renderTargetBitmap.PixelHeight,
                                    Bitmap = await renderTargetBitmap.GetPixelsAsync(),
                                    RelativeTime = totalTimeElapsed
                                };
                                bitmapToDirect3DHelper.AddFrame(frameData);
                            });

                      }

                  }
                  stopwatch.Stop();

              });

            StorageFolder tempFolder = ApplicationData.Current.TemporaryFolder;
            StorageFile tempFile = await tempFolder.CreateFileAsync("temp.mp4", CreationCollisionOption.ReplaceExisting);

            using (var stream = await tempFile.OpenAsync(FileAccessMode.ReadWrite))
            {
                await _encoder.EncodeAsync(
                    stream,
                    framesPerSecond);
            }
            // Encoding is done at this point
            StopRecording();
        }

        private Size DetermineResolutionSize()
        {
            string key = screenResolutionList[ResolutionComboBox.SelectedIndex];
            Size resolutionValue = resolutionDictionary[key];

            // Easy way of determining if in window is portrait
            if (Window.Current.Bounds.Height > Window.Current.Bounds.Width)
            {
                Size portraitResolutionValue = new Size(resolutionValue.Height, resolutionValue.Width);
                return portraitResolutionValue;
            }

            return resolutionValue;
        }

        private void StopRecording()
        {
            hasTimerStopped = true;
            StopButton.IsEnabled = false;
            RecordButton.IsEnabled = true;
            _encoder.Stop();
            bitmapToDirect3DHelper.Initialise();
        }
        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            StopRecording();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            FPSComboBox.SelectedIndex = 0;
            ResolutionComboBox.SelectedIndex = 0;
        }
    }
}
